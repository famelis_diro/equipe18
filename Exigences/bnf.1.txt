﻿
	INTERFACE GRAPHIQUE PRATIQUE
	L'interface du système doit être "user friendly", ergonomique et pratique. L'interface graphique doit être logique et facile à comprendre:
	l'agent doit pouvoir y naviguer facilement. Ses fonctions doivent y être visuellement bien explicitées. Son utilisation
	doit être facile à apprendre pour les agents de #GYM. La formation de nouveaux employés ne doit pas être ralentie par
	l'assimilation de son fonctionnement. L'interface doit être assez compacte pour ne pas qu'elle interfère avec le travail de l'agent.
	La densité d'information par page pourrait par exemple être limitée.

	SÉCURITÉ
	Les données doivent être confidentielles: le système se doit d'être intègre et de protéger l'intimité des membres 
	et des professionnels. Par ailleurs, les données du système sont la propriété exclusive de #GYM. 
	Les agents pourraient être eux-mêmes enregistrés dans le Centre de Données et le système pourrait nécessiter un
	système "login".

	ACCÈS AUX DONNÉES
	Le système doit traiter les requêtes rapidement. On doit pouvoir accéder aux données du
	Centre de Données de manière efficace. L'accès aux numéros de membre doit se faire en temps
	réel afin d'éviter les délais d'attente. Idem pour le numéro du professionnel:
	un professionnel ne doit pas attendre afin d'obtenir son authentification pour créer une séance de service. 
	C'est de même lorsque l'agent accède au Répertoire des Services. Sa consultation doit être quasi-instantanée 
	pour qu'un membre puisse parcourir les services offerts et les séances disponibles. L'agent doit également pouvoir trouver
	rapidement un service à l'aide de son code à sept chiffres. 

	AJOUTER ET MODIFIER DES DONNÉES
	L'interface utilisateur doit permettre de traiter très rapidement les demandes des usagers, car
	le systême doit être utilisé très fréquemment. L'enregistrement d'un nouveau membre doit se faire 
	rapidement pour que le membre adhère à #GYM. L'inscription d'un membre et la confirmation de sa participation
	à une séance doit également se faire en temps réel pour ne pas faire attendre le client. L'enregistrement d'une séance
	doit aussi se faire rapidement pour que les membres puissent commencer à s'y inscrire dès sa création. 

	CAPACITÉ
	Il n'est pas désirable que la capacité du système limite la quantité de client éventuelle de #GYM. L'idéal théorique serait
	qu'on puisse avoir une infinité de membres, de professionnels, de services et de séances répertoriés dans le Centre de Données.
	Par contre, ceci est impossible en pratique. Le Centre de Données et le Répertoire de Servicesdoivent donc pouvoir contenir le 
	plus grand nombre de données possible sans compromettre l'efficacité du logiciel. 
	 


	
	########
	AUTRE IDÉES 
	########

	INTÉGRITÉ:
	le système doit être en mesure de répondre aux erreurs d'input. Il doit être capable de gérer, par exemple, si
	le numéro d'un membre ou d'un professionnel est invalide ou si il ne trouve pas le numéro d'un service dans le 
	Répertoire de Services. 

	DISPONIBILITÉ:
	Le système doit être disponible tout au long des heures d'ouverture de #GYM et doit pouvoir "runner" durant de 
	longues périodes de temps

	FIABILITÉ:
	Le conservation des données doit être assurée par un système robuste. 

	SÉCURITÉ?:
	Intimité des membres et des professionnels. Protection des données appartenant à #GYM.  